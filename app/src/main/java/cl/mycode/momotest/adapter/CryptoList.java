package cl.mycode.momotest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cl.mycode.momotest.R;
import cl.mycode.momotest.models.Crypto;

public class CryptoList extends RecyclerView.Adapter<CryptoList.ViewHolder> {

    private final Context context;
    private final ArrayList<Crypto> arrayList;

    public CryptoList(Context context, ArrayList<Crypto> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_coin_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull CryptoList.ViewHolder holder, int position) {
        Crypto crypto = arrayList.get(position);
        holder.btcName.setText(crypto.getName());
        holder.btcValue.setText(crypto.getValue());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView btcName;
        private final TextView btcValue;
        public ViewHolder(View view) {
            super(view);
            btcName = view.findViewById(R.id.btc_name);
            btcValue = view.findViewById(R.id.btc_value);
        }
    }
}
