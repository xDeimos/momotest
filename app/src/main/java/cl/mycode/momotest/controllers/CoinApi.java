package cl.mycode.momotest.controllers;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class CoinApi extends AsyncTask<String, String, String> {
    private JSONArray data = null;
    private final int start;
    private final int limit;
    private final String convert;
    private final Context context;

    public CoinApi(Context context, int start, int limit, String convert) {
        this.context = context;
        this.start = start;
        this.limit = limit;
        this.convert = convert;
    }

    public void run() {
        doInBackground();
    }

    private RequestParams createParams(int start, int limit, String convert) {
        RequestParams params = new RequestParams();
        params.put("start", String.valueOf(start));
        params.put("limit", String.valueOf(limit));
        params.put("sort", "price");
        params.put("convert", convert);
        return params;
    }

    @Override
    protected String doInBackground(String... strings) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("X-CMC_PRO_API_KEY", "13c063bc-0c56-4272-9ac0-6ddaf0451464");
        String url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest";
        client.get(url, createParams(start, limit, convert), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    data = response.getJSONArray("data");
                    System.out.println(data.toString());
                } catch (JSONException e) {
                    Log.e("Parse to JSONArray", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                Toast.makeText(context, "Failed to load data", Toast.LENGTH_SHORT).show();
            }
        });
        return null;
    }

    public boolean isDataReady() {
        return data != null;
    }

    public JSONArray getData() {
        return data;
    }
}