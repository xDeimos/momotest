package cl.mycode.momotest;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cl.mycode.momotest.adapter.CryptoList;
import cl.mycode.momotest.controllers.CoinApi;
import cl.mycode.momotest.models.Crypto;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Crypto> CryptoArrayList;
    private RecyclerView recyclerView;
    private CoinApi coinApi;
    private Handler customHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CryptoArrayList = new ArrayList<>();
        recyclerView = findViewById(R.id.list_view_recyclerview);
        coinApi = new CoinApi(this, 1, 10, "USD");
        coinApi.run();
        customHandler = new Handler();
        customHandler.postDelayed(checkData, 0);
    }

    private final Runnable checkData = new Runnable() {
        @Override
        public void run() {
            if (!coinApi.isDataReady()) customHandler.postDelayed(this, 2000);
            else {
                try {
                    Toast.makeText(getApplicationContext(), "Data find", Toast.LENGTH_SHORT).show();
                    setViewData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void setViewData() throws JSONException {
        JSONArray jsonArray = coinApi.getData();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            Crypto crypto = new Crypto();
            crypto.setName(object.getString("symbol") + " ("+object.getString("name")+")");
            JSONObject price = object.getJSONObject("quote").getJSONObject("USD");
            crypto.setValue(String.valueOf(price.getDouble("price")));
            CryptoArrayList.add(crypto);
            CryptoList adapter = new CryptoList(MainActivity.this, CryptoArrayList);
            recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            recyclerView.setAdapter(adapter);
        }
    }
}