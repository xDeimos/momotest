package cl.mycode.momotest.models;

public class Crypto {
    String name;
    String value;

    public Crypto() {
    }

    public Crypto(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Crypto{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

}
