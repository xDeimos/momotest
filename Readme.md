# Soy Momo Test

Proyecto creado para la empresa SoyMomo en postulacion laboral.
Requerimientos del proyecto: 

Desarrollar una app Android (Java o Kotlin) para visualizar el valor de las 10 criptomonedas con mayor valor actual

1. Crear repositorio en GitHub o BitBucket
2. Usar arquitectura basada en MVC (o derivados como MVVM)
3. Usar una API para obtener información (ejemplo: https://coinmarketcap.com/api/)
4. Desplegar valores en una lista, diseño a libre elección

Comentar en el readme decisiones tomadas, features que se podrían implementar, posibles bugs, etc...

---

## Cryptocurrency List

El proyecto consta de crear una app que consulta una API que dependiendo de los parametros recibidos y el endpoint indicado filtra informacion obtenida

1. Endpoint: https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest
2. Parametros: [ start = 1, limit = 10, convert = USD, sort = price ].
3. Informacion rescatada: [ Symbol, Name, Price ].

---

## Liberias

Loopj: Permite realizar una llamada asincrona mediante un http request manejando parametros y cabeceras de forma rapida y efectiva.

implementation 'com.loopj.android:android-async-http:1.4.9'

---

## Funcionamiento

La clase principal inicia un proceso en segundo plano que realiza la consulta a la API mediante una libreria de request asincrono. Mientras que en un siguiente hilo verifica cada 2 segundos si es que la informacion ya fue obtenida. Si existe la informacion se muestra en una lista mediante un RecyclerView dentro de la misma pantalla principal.
Para que esto se lleve a cabo se crea una clase adaptador que maneja la informacion hacia un modelo del objeto Crypto creado para la lista el cual posee un nombre y un valor.

1. Inicia llamada a la API.
2. Inicia hilo para validar información recibida.
3. Recibida información, itera JSONArray.
4. Por cada JSONObject rescata symbol, name, price y lo setea en un nuevo objeto Crypto.
5. Se agrega el objeto a un arreglo, se añade al adaptador y el adaptador al RecyclerView.

El objeto de la lista se establece por defecto al valor de USD y mostrado como una CardView por estetica y organizacion.

---

## Bugs y opciones de mejora
### Bugs


1. A falta de conectividad el servicio se cae y no rescata informacion.
2. Error al obtener la imagen de la cripto moneda debido a que forma parte de un endpoint distinto al que da el valor de la cryptomoneda.

### Opciones de mejora

Debido a que el servicio requiere red y consumo de API externa las opciones de mejora se basan en manejo de excepciones y optimizacion de ejecuciones.

1. Control de errores por falta red
2. Establecer timeouts para el llamado a la API por lentitud de servicio.
3. Tarea en segundo plano para llamar a la API de forma periodica y mantener actualizados los valores.
4. Establecer un autocomplete con las opciones de monedas de cambio y hacer una llamada en tiempo real a la API.
5. Llamado inicial a la API meidante boton que gatille la busqueda con moneda indicada.
6. Obtener meta de cryptomoneda y cuando exista la data base cotejar informacion para complementar visual del objeto.


Nota: La opciones de mejora asi como los bugs son opciones que pense en implementar pero omiti para poder generar estas opciones para el documento. Pueden ser trabajadas como evolutivos dentro del mismo proyecto.
